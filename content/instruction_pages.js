instruction_pages = [
`<div style="text-align: left; width: 50em;">
<p>Als u zo doorklikt, zult u terechtkomen op de newsfeed van een gesimuleerde
Twitter-omgeving. U bent uitgenodigd om in deze omgeving per tweet die u ziet
drie handelingen te verrichten. Deze handelingen, waarvan er twee optioneel
zijn en één verplicht, worden op de komende pagina's toegelicht. Het is
belangrijk dat u deze informatie goed leest.</p></div>`,
`<div style="text-align: left; width: 50em;">
<p>1. U bent uitgenodigd de tweets die u ziet te retweeten. U kunt dit doen op
dezelfde manier waarop u op Twitter een bericht retweet; u klikt op de twee
gebogen pijltjes onder de desbetreffende tweet. Het retweeten van tweets is
geen verplichting; hanteert u voor het retweeten van deze tweets dezelfde
criteria als u voor te retweeten tweets in uw echte Twitter-omgeving hanteert.
Er is in dit experiment geen limiet aan het aantal tweets dat u kunt retweeten.
De tweets die u retweet, komen niet op uw echte Twitter-account.</p></div>`,
`<div style="text-align: left; width: 50em;">
<p>2. U bent uitgenodigd de tweets die u ziet te favouriten. U kunt dit doen op
dezelfde manier waarop u op Twitter een bericht favouritet; u klikt op het
hartje onder de desbetreffende tweet. Het favouriten van tweets is geen
verplichting; hanteert u voor het favouriten van deze tweets dezelfde criteria
als u voor te favouriten tweets in uw echte Twitter-omgeving hanteert. Er is in
dit experiment geen limiet aan het aantal tweets dat u kunt favouriten. De
tweets die u favouritet, komen niet op uw echte Twitter-account.</p></div>`,
`<div style="text-align: left; width: 50em;">
<p>3. Twitter wil van elke tweet op deze newsfeed weten of nepnieuws zich erin
bevindt. Nepnieuws is informatie die onwaar is. U wordt bij dezen verzocht bij
elke tweet op een vierpunts-schaal aan te geven in welke mate u denkt dat de
betreffende tweet nepnieuws bevat.</p>
<ul>
<li>Eén vierkantje inkleuren betekent ‘Deze tweet bevat geen nepnieuws’.</li>
<li>Twee vierkantjes inkleuren betekent ‘Een klein deel van de informatie in deze tweet is nepnieuws’.</li>
<li>Drie vierkantjes inkleuren betekent ‘Een groot deel van de informatie in deze tweet is nepnieuws’.</li>
<li>Vier vierkantjes inkleuren betekent ‘Alle informatie in deze tweet is nepnieuws’.</li>
</ul>
Een voorbeeld is de volgende tweet.
<img width=800 src="content/tweetzondernepnieuws.png">

<p>Bovenstaande is een voorbeeld van een tweet zoals deze aan u gepresenteerd
wordt op de newsfeed. Onderstaande is dezelfde tweet, waarbij u uw oordeel ten
aanzien van nepnieuws in de tweet hebt aangegeven. U hebt geïndiceerd dat een
groot deel van de informatie in deze tweet nepnieuws is.</p>
<img width=800 src="content/tweetmetnepnieuws.png">
<p>Het experiment is afgelopen als u van elke tweet hebt aangegeven in welke mate
u denkt dat de tweet nepnieuws bevat.</p></div>`,
`<div style="text-align: left; width: 50em;">
<p>Om de instructies samen te vatten: in de gesimuleerde Twitter-omgeving wordt u uitgenodigd om</p>
<ol>
<li>de tweets te retweeten (optioneel);</li>
<li>de tweets te favouriten (optioneel);</li>
<li>van elke tweet aan te geven of deze nepnieuws bevat (verplicht).</li>
</ol>
<p>Op het experiment volgt nog een enkele vraag.</p>
<p>Klikt u op ‘next’ om het experiment te starten.</p></div>`
]
